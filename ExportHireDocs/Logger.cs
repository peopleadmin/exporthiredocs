﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportHireDocs
{
    static class Logger
    {
        public static string LogString;

        public static void SaveLog(bool Append = true, string Path = "./Log.txt")
        {
            if (Logger.LogString != null && Logger.LogString.Length > 0)
            {
                if (!Append)
                {
                    using (StreamWriter file = new StreamWriter(Path))
                    {
                        file.Write(Logger.LogString);
                        file.Close();
                        file.Dispose();
                    }
                }
                else
                {
                    using (StreamWriter file = File.AppendText(Path))
                    {
                        file.Write(Logger.LogString);
                        file.Close();
                        file.Dispose();
                    }
                }
            }
        }

        public static void Write(string str)
        {
            Console.Write(str);
            string logString = Logger.LogString;
            DateTime now = DateTime.Now;
            Logger.LogString = string.Concat(logString, string.Format("{0} - {1}", now.ToString(), str));
        }

        public static void WriteLine(string str)
        {
            Console.WriteLine(str);
            string logString = Logger.LogString;
            DateTime now = DateTime.Now;
            Logger.LogString = string.Concat(logString, string.Format("{0} - {1} \n", now.ToString(), str));
        }
    }
}
