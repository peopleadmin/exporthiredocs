﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using System.Configuration;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace ExportHireDocs
{
    public class Export
    {
        private const string bucketName = "netchemia";

        private const string keyName = "494/970/Hire/";

        private RegionEndpoint bucketRegion;

        private const string awsAccessKey = "AKIAJVIELJO7VRF5VBFA";

        private const string awsSecretKey = "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";

        private string connectionstring;

        private IAmazonS3 client;

        private string foldername;

        private string filename;

        private string s3filename;

        private string directory;

        public Export()
        {
            bucketRegion = RegionEndpoint.USEast1;
            connectionstring = ConfigurationManager.ConnectionStrings["connectionstring"].ToString();
            foldername = string.Empty;
            filename = string.Empty;
            s3filename = string.Empty;
            directory = ConfigurationManager.AppSettings["basedirectory"].ToString();
        }

        public void Start()
        {
            SqlConnection con = new SqlConnection(connectionstring);
            string whereclause = ConfigurationManager.AppSettings["whereclause"].ToString();
            string str = string.Concat("SELECT DISTINCT H.ApplicantProfileID,FirstName +' '+ LastName +' '+ MiddleName +' '+ DateOfBirth As Name FROM vHires_Approved H JOIN ApplicantProfile_Header AH on AH.ApplicantProfileID = H.ApplicantProfileID ", whereclause, " ORDER BY ApplicantProfileID");
            Logger.WriteLine(str);
            SqlCommand sqlCommand = new SqlCommand(str, con);
            con.Open();
            SqlDataReader rdr = sqlCommand.ExecuteReader();
            while (rdr.Read())
            {
                int applicantProfileID = Convert.ToInt32(rdr["ApplicantProfileID"]);
                foldername = rdr["Name"].ToString().TrimEnd();
                Logger.WriteLine(string.Concat(new object[] { "Start : ", foldername, " ID: ", applicantProfileID }));
                getFiles(string.Concat("SELECT BinaryFileID,BF.FileName,SRFileType,FileContents,ApplicantProfileID,ParentID,S3Syncd FROM Documents D Inner Join BinaryFiles BF on(D.DocumentID= BF.ParentID)AND BF.ParentTable = 'Documents'WHERE D.ApplicantProfileID in (", applicantProfileID, ") AND BF.AjaxFinalized = 'T' AND BF.SRFileType = 'Admin_SupportingDocument'"), "HRDocs");
                Logger.WriteLine("--------------------------------------------------------------END-----------------------------------------------------------------------------");
                Logger.SaveLog(true, "./Log.txt");
            }
        }

        public void getFiles(string sqlQuery, string type)
        {
            string S3Syncd = string.Empty;
            SqlConnection con = new SqlConnection(connectionstring);
            SqlCommand sqlCommand = new SqlCommand(sqlQuery, con);
            con.Open();
            SqlDataReader rdr = sqlCommand.ExecuteReader();
            if (rdr.HasRows)
            {
                Logger.WriteLine(type);
            }
            else
            {
                Logger.WriteLine(string.Concat(type, " : No records"));
            }
            while (rdr.Read())
            {
                s3filename = rdr["BinaryFileID"].ToString();
                filename = MakeValidFileName(rdr["FileName"].ToString());
                S3Syncd = rdr["S3Syncd"].ToString();
                string filepath = getFilePath(filename);
                try
                {
                    if (!string.IsNullOrEmpty(S3Syncd))
                    {
                        s3filename = string.Concat(s3filename, Path.GetExtension(filename));
                        getS3file(filepath);
                    }
                    else
                    {
                        File.WriteAllBytes(filepath, (byte[])rdr["FileContents"]);
                    }
                }
                catch (Exception exception)
                {
                    Logger.WriteLine(string.Concat(s3filename, ", ", filename));
                    Logger.WriteLine(exception.ToString());
                }
            }
            con.Close();
        }

        private string MakeValidFileName(string name)
        {
            string invalidChars = Regex.Escape(new string(Path.GetInvalidFileNameChars()));
            string invalidRegStr = string.Format("([{0}]*\\.+$)|([{0}]+)", invalidChars);
            return Regex.Replace(name, invalidRegStr, "_");
        }

        private string getFilePath(string name)
        {
            if (File.Exists(string.Concat(directory, foldername, "\\", name)))
            {
                name = string.Concat(Path.GetFileNameWithoutExtension(name), "_1", Path.GetExtension(filename));
                getFilePath(name);
            }
            return string.Concat(directory, foldername, "\\", name);
        }

        public async void getS3file(string filepath)
        {
            try
            {
                client = new AmazonS3Client("AKIAJVIELJO7VRF5VBFA", "qT9QdppAjbuvmhrVB0D+oRjT/aAdLdZ2xe+2WMR/", RegionEndpoint.USEast1);
                GetObjectRequest request = new GetObjectRequest()
                {
                    BucketName = bucketName,
                    Key = string.Concat(keyName, s3filename)
                };

                TransferUtility fileTransferUtility = new TransferUtility(client);
                fileTransferUtility.Download(filepath, request.BucketName, request.Key);
            }
            catch(Exception ex)
            {
                Logger.WriteLine("error downloading file from S3"+ex.ToString());
            }

        }

    }
}
